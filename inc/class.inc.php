<?php
class Guide_DB {
	function Select($table,$name='fid',$ck='',$prview='',$url='',$fid=0,$toclass='',$multiple='',$size=6,$ctype='',$fiddb='',$tistr='现有分类'){
		global $db;
		if($url!="" && !empty($url)){
			$reto=" onchange=\"window.location=('{$url}&{$name}='+this.options[this.selectedIndex].value+'')\"";
		}elseif($ctype!="" && !empty($ctype)){
			 
			 $reto=" onchange=\"(this.options[this.selectedIndex].value=='-1')&&alert('你不能选择这个栏目,只能选择黑色字体的栏目');\" ";
		}
		if($multiple){
			$multiple=" size=$size multiple=multiple ";
		}
		if ($tistr==""){$tistr="现有分类";}
		$show="<select name='$name' $reto $multiple><option value=''>{$tistr}</option>";
		if ($prview=="prview"){$show.=$this->SelectIn($table,$fid,$ck,$toclass,$ctype,$fiddb);}
		else{$show.=$this->SelectIn2($table,$fid,$ck,$toclass,$ctype,$fiddb);}
		$show.=" </select>";
		return $show;
	}
	
	//前台显示分类
	function Selectshow($table,$name='fid',$fid=0,$foption="所有分类"){
		global $db,$ARTCLASS,$ARTCLASSNAME;
		$show="<select name='$name' ><option value=''>$foption</option>";
		if ($fid==""){$fid=0;}
		$fid=explode(",",$fid);
		foreach ($fid as $key => $value){
		  $show.=$this->SelectshowIn($table,$value);
		}
		$show.=" </select>";
		return $show;
	}
	function SelectshowIn($table,$fid) {
		global $db,$lfjdb;
		$query=$db->excu("select fid,fup,class,name,type from $table where fup in ($fid)  order by list,fid");
		while( @extract($db->fetch_array($query)) ){
			$topico=$this->BlankShowIcon($class);
			$cid=$fid;
			$show.="<option value='$cid' $ckk style='color:$color'>{$topico}$name</option>";
			$show.=$this->SelectshowIn($table,$fid);
		}
		return $show;
	}
	function BlankShowIcon($class,$blank='1'){
		if($blank){
			for($i=1;$i<$class;$i++){
				$show.="&nbsp;&nbsp;";
			}
		}
			return "{$show}|_";
	}
	//前台显示分类
	
	function Checkbox($table,$name='fiddb[]',$fiddb=array(),$fup=0,$w=350,$h=200){
		$fid_str = $this->CheckboxIn($table,$name,$fiddb,$fup);
		return "<div style='width:$w;height:$h;overflow:auto;border:1px dotted #ccc;background:#FAFAFA;'>$fid_str</div>";
	}
	function CheckboxIn($table,$name,$fiddb,$fup=0){
		global $db;
		$query = $db->query("SELECT fid,name,fup,class FROM $table WHERE fup='$fup' ORDER BY list");
		while($rs = $db->fetch_array($query)){
			$icon="";
			for($i=1;$i<$rs['class'];$i++){
				$icon.="&nbsp;|&nbsp;&nbsp;&nbsp;&nbsp;";
			}
			if($icon){
				$icon=substr($icon,0,-24);
				$icon.="--";
			}
			$ck=in_array($rs[fid],$fiddb)?'checked':'';
			$show.="<input type='checkbox' name='$name' value='$rs[fid]' $ck>{$icon}【{$rs[name]}】<br>";
			$show .= $this->CheckboxIn($table,$name,$fiddb,$rs[fid]);
		}
		return $show;
	}
	/*导航条缓存*/
	function GuideFidCache($table,$filename="guide_fid.php",$TruePath=0){
		global $db,$webdb;
		$show="<?php \r\n";
		//$showindex="<a href='javascript:guide_link(0);' class='guide_menu'>&gt;首页</a>";
		$showindex="<a href='\$webdb[www_url]' class='guide_menu'>&gt;首页</a>";
		$query=$db->query("select fid,name from $table ");
		while( @extract($db->fetch_array($query)) ){
			$show.="\$GuideFid[$fid]=\"$showindex".$this->SortFather($table,$fid)."\";\r\n";
		}
		$show.=$shows.'?>';
		if($TruePath==1){
			write_file($filename,$show);
		}else{
			write_file(PHP168_PATH."php168/$filename",$show);
		}
	}
	/**/
	function FidSonCache($table,$filename="fidson_menu.js",$TruePath=0){
		global $db,$N_path,$webdb;
		/*启用下拉菜单，栏目大于100个时不推荐*/
		if($webdb[nav_menu]){
			$show="var FidSon_0=\"".$this->SortSon($table,$url,0,0)."\";\r\n";
			$query=$db->query("select fid from $table where sons>0 ");
			while( @extract($db->fetch_array($query)) ){
				$show.="var FidSon_$fid=\"".$this->SortSon($table,$fid)."\";\r\n";
			}
			if($TruePath==1){
				write_file($filename,$show);
			}else{
				write_file(PHP168_PATH."php168/$filename",$show);
			}
			
		}
	}
	/**/
	function BlankIcon($class,$blank='1'){
		if($blank){
			for($i=1;$i<$class;$i++){
				$show.="&nbsp;&nbsp;";
			}
		}
		$detail=array("■■","■","◆","◇","▲","△","★","☆","⊙","※","卐","◎","●","□");
		if($detail[$class]){
			return "{$show}$detail[$class]";
		}
		else{
			return "{$show}【{$class}】";
		}
	}
	/**/
	function SelectIn($table,$fid,$ck,$toclass,$ctype,$fiddb) {
		global $db,$lfjdb;
		if ($toclass!=""){$toclassstr=" and class<'$toclass'";}
		$query=$db->excu("select fid,fup,class,name,type from $table where fup='$fid' $toclassstr order by list,fid");
		while( @extract($db->fetch_array($query)) ){
			$topico=$this->BlankIcon($class);
			if(is_array($ck)){
				in_array($fid,$ck)?$ckk='selected':$ckk='';
			}else{
				$ck==$fid?$ckk='selected':$ckk='';
			}
			$color='';
			$cid=$fid;

			  global $fiddbadmin2;
			  if (strpos("#**#$fiddbadmin2","|||$cid|||")>0){
				$show.="<option value='$cid' $ckk style='color:$color'>{$topico}$name</option>";
				$show.=$this->SelectIn2($table,$fid,$ck,$toclass,$ctype,$fiddb);
			  }
			  else{
			      $show.=$this->SelectIn($table,$fid,$ck,$toclass,$ctype,$fiddb);
			  }
		}
		return $show;
	}

	/**/
	function SelectIn2($table,$fid,$ck,$toclass,$ctype,$fiddb) {
		global $db,$lfjdb;
		if ($toclass!=""){$toclassstr=" and class<'$toclass'";}
		$query=$db->excu("select fid,fup,class,name,type from $table where fup='$fid' $toclassstr order by  list,fid");
		while( @extract($db->fetch_array($query)) ){
			$topico=$this->BlankIcon($class);
			if(is_array($ck)){
				in_array($fid,$ck)?$ckk='selected':$ckk='';
			}else{
				$ck==$fid?$ckk='selected':$ckk='';
			}
			$color='';
			$cid=$fid;
			$show.="<option value='$cid' $ckk style='color:$color'>{$topico}$name</option>";
			$show.=$this->SelectIn2($table,$fid,$ck,$toclass,$ctype,$fiddb);
		}
		return $show;
	}
	/**/
	function SortFather($table,$fup) {
		global $db,$webdb;
		$query=$db->query("select fid,fup,class,name,sons from $table where fid='$fup'");
		while( @extract($db->fetch_array($query)) ){
			if($webdb[nav_menu]&&$sons){			//启用下拉菜单，大网站不推荐
				$showmenu="onMouseOver='ShowMenu_mmc(forum_$fid,100)' onMouseOut='HideMenu_mmc()'";
			}
			$name=str_Replace('"',"",$name);
			//$show.=" -&gt; <a $showmenu href='javascript:guide_link($fid);' class='guide_menu'>$name</a>";
			$show.=" -&gt; <a $showmenu href='list.php?fid=$fid' class='guide_menu'>$name</a>";
			$show=$this->SortFather($table,$fup).$show;
		}
		return $show;
	}
	/**/
	function SortSon($table='',$fid='0',$showsons=1){
		global $db;
		$query=$db->query("select fid,name,sons,class from $table where fup='$fid' order by sons");
		while( @extract($db->fetch_array($query)) ){
			$topico=$this->BlankIcon($class);
			$name=str_Replace('"',"",$name);
			//$show.="{$topico}<a href='javascript:guide_link($fid);'>$name</a><br>";
			$show.="{$topico}<a href='list.php?fid=$fid'>$name</a><br>";
			if($showsons){
				$show.=$this->SortSon($table,$fid,$showsons);
			}
						
		}
		return $show;
	}
}
?>