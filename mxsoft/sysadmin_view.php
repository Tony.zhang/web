<?PHP
$PurviewLevel=0;

require("../inc/common.inc.php");
require_once(dirname(__FILE__)."/"."Admin_ChkPurview.php");
/**
*服务器信息
**/
function systemMsg(){
	global $db,$siteurl,$onlineip,$SCRIPT_FILENAME,$WEBURL;
	if(mysql_get_server_info()<'4.1'){
		$rs[mysqlVersion]=mysql_get_server_info()."(低版本);";
	}else{
		$rs[mysqlVersion]=mysql_get_server_info()."(高版本);";
	}
	isset($_SESSION) ? $rs[ifcookie]="SUCCESS" : $rs[ifcookie]="FAIL";
	$rs[sysversion]=PHP_VERSION;	//PHP版本
	$rs[max_upload]= ini_get('upload_max_filesize') ? ini_get('upload_max_filesize') : 'Disabled';	//最大上传限制
	$rs[max_ex_time]=ini_get('max_execution_time').' 秒';	//最大执行时间
	$rs[sys_mail]= ini_get('sendmail_path') ? 'Unix Sendmail ( Path: '.ini_get('sendmail_path').')' :( ini_get('SMTP') ? 'SMTP ( Server: '.ini_get('SMTP').')': 'Disabled' );	//邮件支持模式
	$rs[systemtime]=date("Y-m-j g:i A");	//服务器所在时间
	$rs[onlineip]=$onlineip;				//当前IP
	if( function_exists("imagealphablending") && function_exists("imagecreatefromjpeg") && function_exists("ImageJpeg") ){
		$rs[gdpic]="支持";
	}else{
		$rs[gdpic]="不支持";
	}
	$rs[allow_url_fopen]=ini_get('allow_url_fopen')?"On 支持采集数据":"OFF 不支持采集数据";
	$rs[safe_mode]=ini_get('safe_mode')?"打开":"关闭";
	$rs[DOCUMENT_ROOT]=$_SERVER["DOCUMENT_ROOT"];	//程序所在磁盘物理位置
	$rs[SERVER_ADDR]=$_SERVER["SERVER_ADDR"];		//服务器IP
	$rs[SERVER_PORT]=$_SERVER["SERVER_PORT"];		//服务器端口
	$rs[SERVER_SOFTWARE]=$_SERVER["SERVER_SOFTWARE"];	//服务器软件
	$rs[SCRIPT_FILENAME]=$_SERVER["SCRIPT_FILENAME"];	//当前文件路径
	$rs[SERVER_NAME]=$_SERVER["SERVER_NAME"];	//域名

	//获取ZEND的版本
	ob_end_clean();
	ob_start();
	phpinfo();
	$phpinfo=ob_get_contents();
	ob_end_clean();
	ob_start();
	preg_match("/with(&nbsp;| )Zend(&nbsp;| )Optimizer(&nbsp;| )([^,]+),/is",$phpinfo,$zenddb);
	$rs[zendVersion]=$zenddb[4]?$zenddb[4]:"未知/可能没安装";
	
	return $rs;
}
$systemMsg=systemMsg();
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=gb2312" />
<meta http-equiv="X-UA-Compatible" content="IE=EmulateIE7" />
<TITLE>铭讯EOS网站后台管理</TITLE>
<Meta name="Keywords" Content="济南网站建设 济南网站制作 山东网站建设 济南铭讯软件有限公司">
<Meta name="Description" Content="济南网站制作：铭讯软件专业服务优秀企业，致力于网站建、设网站推广、网络营销一体的全访位网络服务商">
<link href="images/css.css" rel="stylesheet" type="text/css">
</head>

<body>
<?php require("head.php") ;?>
<div class="boxdiv">
<table width="100%" cellpadding="0" cellspacing="0" align="center" border="0"><tr><td height="10"></td></tr></table>
<table cellpadding="2" cellspacing="1" border="0" width="95%" align="center" class="table_southidc1" >
  <tr class="head"> 
    <td height="25" align="center" colspan="2"><b>管理快捷方式</b></td>
  </tr>
  <tr class="list"> 
    <td width="15%" align="center"	 height="23"><b>快捷功能链接</b></td>
    <td width="85%" height="23"><a href="center.php?job=config">网站设置</a> 
      | <a href="Admin_menber.php">管理员管理</a> </td>
  </tr>
</table>
<br>
<table cellpadding="2" cellspacing="1" border="0" width="95%" align="center" class="table_southidc">
  <tr class="head"> 
    <td colspan="2" height="25" align="center"><b>管 理 员 信 息</b></td>
  </tr>
  <tr bgcolor="#FFFFFF">
    <td id="map" width="50%">用户名：<?php echo $_SESSION['mxadmin']['username']?></td>
    <td id="map" width="50%">管理级别：<?php if ($_SESSION['mxadmin']['purview']==1 ){echo "超级管理员";} else {echo "普通管理员";}?></td>
  </tr>
  <tr bgcolor="#FFFFFF"> 
    <td id="map" width="50%">登陆IP：<?php echo $_SERVER['REMOTE_ADDR']?></td>
    <td id="map" width="50%">登录次数：
	<?php
	$lognum=$db->excu("SELECT LoginTimes FROM members WHERE username='".$_SESSION['mxadmin']['username']."' ");
	$lognum=mysql_fetch_array($lognum);
	echo $lognum[0];
	?></td>
  </tr>
</table>
<table cellpadding="2" cellspacing="1" border="0" width="95%" align="center" class="table_southidc">
  <tr class="head"> 
    <td height="23" colspan="2"><b>服 务 器 信 息</b></td>
  </tr>
  <tr bgcolor="#FFFFFF"> 
    <td id="map" width="50%">服务器端信息：<?php echo $systemMsg[SERVER_SOFTWARE]?></td>
    <td id="map" width="50%">邮件支持模式：<?php echo $systemMsg[sys_mail]?></td>
  </tr>
  <tr bgcolor="#FFFFFF"> 
    <td id="map" width="50%">PHP程式版本：<?php echo $systemMsg[sysversion]?></td>
    <td id="map" width="50%">服务器IP：<?php echo $systemMsg[SERVER_ADDR]?></td>
  </tr>
  <tr bgcolor="#FFFFFF"> 
    <td id="map" width="50%">MYSQL 版本：<?php echo $systemMsg[mysqlVersion]?></td>
    <td id="map" width="50%">服务器端口:<?php echo $systemMsg[SERVER_PORT]?></td>
  </tr>
  <tr bgcolor="#FFFFFF"> 
    <td id="map" width="50%">最大上传限制：<?php echo $systemMsg[max_upload]?></td>
    <td id="map" width="50%">服务器所在时间：<?php echo $systemMsg[systemtime]?></td>
  </tr>
  <tr bgcolor="#FFFFFF"> 
    <td id="map" width="50%">最大执行时间：<?php echo $systemMsg[max_ex_time]?></td>
    <td id="map" width="50%">网站所在磁盘物理位置：<?php echo $systemMsg[DOCUMENT_ROOT]?></td>
  </tr>
  <tr bgcolor="#FFFFFF"> 
    <td id="map" width="50%">是否允许打开远程文件(采集数据):<?php echo $systemMsg[allow_url_fopen]?></td>
    <td id="map" width="50%">当前文件:<?php echo $systemMsg[SCRIPT_FILENAME]?></td>
  </tr>
  <tr bgcolor="#FFFFFF"> 
    <td id="map" width="50%">图像GD支持与否:<?php echo $systemMsg[gdpic]?></td>
    <td id="map" width="50%">你的IP：<?php echo $systemMsg[onlineip]?></td>
  </tr>
  <tr bgcolor="#FFFFFF"> 
    <td id="map" width="50%">Zend Optimizer版本(PHP加密解释器):<?php echo $systemMsg[zendVersion]?></td>
    <td id="map" width="50%">当前域名：<?php echo $systemMsg[SERVER_NAME]?></td>
  </tr>
</table>
<?php require("foot.php") ;?>
