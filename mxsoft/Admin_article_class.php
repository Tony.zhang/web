<?php
@session_start();
$PurviewLevel=2;
$CheckChannelID=0;  
$PurviewLevel_Others="wenzhanlei";

require("../inc/common.inc.php");
require_once(dirname(__FILE__)."/"."Admin_ChkPurview.php");



?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=gb2312" />
<meta http-equiv="X-UA-Compatible" content="IE=EmulateIE7" />
<TITLE>铭讯EOS网站后台管理</TITLE>
<Meta name="Keywords" Content="济南网站建设 济南网站制作 山东网站建设 济南铭讯软件有限公司">
<Meta name="Description" Content="济南网站制作：铭讯软件专业服务优秀企业，致力于网站建、设网站推广、网络营销一体的全访位网络服务商">
<link rel="stylesheet" href="images/css.css" type="text/css">
</head>
<script>
function checkclass(){
    if (document.form.name.value==""){
	    alert('栏目名称不能为空');
		return false;
	}
	<?php if ($Purview!=1){?>
    if (document.form.fup.value==""){
	    alert('请选择所属分类');
		return false;
	}
	<?php }?>
}

function uppic2(url,name,size,b){
	get_obj('classflashurl').value=url;
}
function uppic3(url,name,size,b){
	get_obj('flashurl').value=url;
}
</script>
<body>
<?php require("head.php");?>
<div class="boxdiv">
<table width="100%" border="0" cellpadding="0" cellspacing="0" class="n_toptable">
<tr><td align="center" colspan="2">
    <table border="0" cellpadding="0" cellspacing="0" class="n_toptabale_s">
    <tr>
      <td width="14" class="lefttd"></td>
      <td  class="centertd">后台用户管理</td>
      <td class="righttd"></td>
     </tr>
    </table>
</td></tr>
</table>
<table border="0" cellpadding="0" cellspacing="1" class="table_southidc" style="width:100%">
  <tr class="list"> 
    <td width="10%" align="center"><strong>管理导航：</strong></td>
    <td align="left">&nbsp;<a href="Admin_article_class.php">栏目管理</a> | <a href="Admin_article_class.php?action=add">添加栏目</a><!-- | <a href="Admin_article_class.php?action=toget">合并栏目</a>-->
	</td>
  </tr>
</table>

<?php
$action=trim($_REQUEST['action']);
if ($action=="saveadd"){
//新类，添加到数据库
  $fup=trim($_POST['fup']);
  $name=trim($_POST['name']);
  if ($Purview!=1 and intval($fup)==0){
  	  $fun->popmassage("您没有对此分类的管理权限，系统将自动返回！","","popback");
	  exit();
  }
  if (strpos("#**#$fiddbadmin2","|||$fup|||")<=0 and  $Purview!=1){
		$fun->popmassage("您没有对所选分类的管理权限","","popback");
		exit();
  }
  if ($name==""){
  	$fun->popmassage("您没有填写添加新栏目的名称，请重新输入！","","popback");
	exit();
  }
	if($fup){
		$rs=$db->get_one("SELECT name,class FROM qh_articleclass WHERE fid='$fup' ");
		$class=$rs['class'];
		$db->excu("UPDATE qh_articleclass SET sons=sons+1 WHERE fid='$fup'");
		$type=0;
	}else{
		$type=1;	/*分类标志*/
		$class=0;
	}
	$class++;
    if ($indexorderid==""){$indexorderid=666;}
    if ($orderid==""){$orderid=666;}
    if ($listorder==""){$listorder=666;}
    if ($fup==""){$fup=0;}
    if ($ispage==""){$ispage=0;}
    if ($ishome==""){$ishome=0;}
    if ($iskeshi==""){$iskeshi=0;}
    if ($iscunju==""){$iscunju=0;}
    if ($isdown==""){$isdown=0;}
    if ($ishome2==""){$ishome2=0;}
	$classshowtype=intval($classshowtype);
	//$descrip=dvHTMLEncode($descrip);
	if ($Purview==1){
	    $db->excu("INSERT INTO qh_articleclass (name,fup,class,classflashurl,flashurl,list,listorder,jumpurl,classtype,classshowtype,ispage,iskeshi,ishome,iscunju,isdown,ishome2,keywords,descrip) VALUES ('$name','$fup','$class','$classflashurl','$flashurl','$orderid','$listorder','$jumpurl','$classtype','$classshowtype','$ispage','$iskeshi','$ishome','$iscunju','$isdown','$ishome2','$keywords','$descrip') ");
	}
	else{
	    $db->excu("INSERT INTO qh_articleclass (name,fup,class,list,listorder,jumpurl,classtype,classshowtype,ispage,iskeshi,ishom,iscunju,isdown,ishome2,keywords,descrip) VALUES ('$name','$fup','$class','$orderid','$listorder','$jumpurl','$classtype','$classshowtype','$ispage','$iskeshi','$ishome','$iscunju','$isdown','$ishome2','$keywords','$descrip') ");
	}
	@extract($db->get_one("SELECT fid FROM qh_articleclass ORDER BY fid DESC LIMIT 0,1"));
	
	mod_sort_class("qh_articleclass",0,0);		//更新class
	mod_sort_sons("qh_articleclass",0);			//更新sons

	jump("创建成功","Admin_article_class.php");

}

elseif ($action=="editclass"){
    $fid=trim($_REQUEST['fid']);
    if ($fid=="" || empty($fid)){
  	  $fun->popmassage("非法访问，系统将自动返回！","","popback");
	  exit();
    }
    if (strpos("#**#$fiddbadmin2","|||$fid|||")<=0){
		$fun->popmassage("您没有对所选分类的管理权限","","popback");
		exit();
    }
	$rsdb=$db->get_one("SELECT * FROM qh_articleclass WHERE fid='$fid'");
	if ($Purview!=1 and intval($rsdb["fup"])==0){
  	  $fun->popmassage("您没有对此分类的管理权限，系统将自动返回！","","popback");
	  exit();
	}
	$sort_fid=$Guidedb->Select("qh_articleclass","fid",$fid,"prview","Admin_article_class.php?action=editclass","",4);
	$sort_fup=$Guidedb->Select("qh_articleclass","fup",$rsdb[fup],"prview","","",3);
	$classtype[$rsdb["classtype"]]=" selected";
	$classshowtype[$rsdb["classshowtype"]]=" selected";
	$iskeshi[$rsdb["iskeshi"]]="checked";
	$ishome[$rsdb["ishome"]]="checked";
	$iscunju[$rsdb["iscunju"]]="checked";
	$isdown[$rsdb["isdown"]]="checked";
?>
<div class="n_div" style="width:100%;">
<table border="0" cellspacing="1" cellpadding="3" width="100%" class="table_southidc">
  <form name="form" method="post" action="Admin_article_class.php?action=saveedit" enctype="multipart/form-data" onSubmit="return checkclass();">
    <tr align="center"> 
      <td colspan="2" class="head">栏目修改</td>
    </tr>
    <tr class="list"> 
      <td width="30%" align="right">当前栏目:</td>
      <td width="70%"><?php echo $sort_fid;?></td>
    </tr>
    <tr class="list"> 
      <td align="right">栏目名称:</td>
      <td><input type="text" name="name" value="<?php echo $rsdb[name];?>" /></td>
    </tr>
	<?php if ($rsdb[fup]!=-1){ ?>
    <tr class="list"> 
      <td align="right">所属分类:</td>
      <td><?php echo $sort_fup?></td>
    </tr>
	<?php }?>
    <tr class="list"> 
      <td align="right">关 键 字:</td>
      <td><input type=text name="keywords" size=70 value="<?php echo trim($rsdb["keywords"])?>"></td>
    </tr>
    <tr class="list"> 
      <td align="right">栏目简介:</td>
      <td><textarea name="descrip" cols="70" rows="3"><?php echo $rsdb["descrip"] ?></textarea></td>
    </tr>
    <tr class="list"> 
      <td align="right">栏目排序:</td>
      <td><input type=text name="orderid" size=20 value="<?php echo $rsdb["list"]?>"></td>
    </tr>
    <tr class="list"> 
      <td align="right">栏目类型:</td>
      <td><select name="classtype">
	  <option value="0" <?php echo $classtype[0]?>>列表</option>
	  <option value="1" <?php echo $classtype[1]?>>图片</option>
	  <option value="4" <?php echo $classtype[4]?>>图片列表</option>
	  <option value="2" <?php echo $classtype[2]?>>单页</option>
	  <option value="3" <?php echo $classtype[3]?>>外链</option>
	  </select>	  </td>
    </tr>
    <!--<tr class="list"> 
      <td align="right">栏目样式:</td>
      <td><select name="classshowtype">
	  <option value="0" <?php echo $classshowtype[0]?>>单列</option>
	  <option value="1" <?php echo $classshowtype[1]?>>双列</option>
	  </select></td>
    </tr>-->
    <tr class="list"> 
      <td align="right">二级页排序:</td>
      <td><input type=text name="listorder" size=20 value="<?php echo $rsdb["listorder"]?>"></td>
    </tr>
    <tr class="list"> 
      <td align="right">外链地址:</td>
      <td><input type=text name="jumpurl" size=50 value="<?php echo trim($rsdb["jumpurl"])?>"><br />栏目选择外链此链接有效</td>
    </tr>
	<tr class="list"> 
      <td align="right">其它属性:</td>
      <td>
        <input type="checkbox" name="ishome2" value="1" <?php if($rsdb[ishome2]==1){echo "checked";}?> class="input_check"/>
      二级主页&nbsp;<input type="checkbox" name="isdown" value="1" <?php if($rsdb[isdown]==1){echo "checked";}?> class="input_check"/>在线下载</td>
    </tr>
    <tr class="list" align="center"> 
      <td colspan="2" class="list"> 
        <input type="submit" name="Submit" value="提交">      </td>
    </tr>
  </form>
</table>
</div>
<?php
}

elseif ($action=="saveedit"){
//保存修改设置
	//检查父栏目是否有问题
    if ($fid=="" || empty($fid)){
  	  $fun->popmassage("请选择要修改的栏目！","","popback");
	  exit();
    }
	
    if (strpos("#**#$fiddbadmin2","|||$fid|||")<=0){
		$fun->popmassage("您没有对所选分类的管理权限","","popback");
		exit();
    }
	check_fup("qh_articleclass",$fid,$fup);

	$rs_fid=$db->get_one("SELECT * FROM qh_articleclass WHERE fid='$fid'");

	if ($Purview!=1 and intval($rs_fid["fup"])==0){
  	  $fun->popmassage("您没有对此分类的管理权限，系统将自动返回！","","popback");
	  exit();
	}

	if($rs_fid[fup]!=$fup)
	{
		$rs_fup=$db->get_one("SELECT class FROM qh_articleclass WHERE fup='$fup' ");
		$newclass=$rs_fup['class']+1;
		$db->excu("UPDATE qh_articleclass SET sons=sons+1 WHERE fup='$fup' ");
		$db->excu("UPDATE qh_articleclass SET sons=sons-1 WHERE fup='$rs_fid[fup]' ");
		$SQL=",class=$newclass";
	}

    if ($orderid==""){$orderid=666;}
    if ($listorder==""){$listorder=666;}
    if ($fup==""){$fup=0;}
    if ($listorder==""){$listorder=666;}
    if ($ispage==""){$ispage=0;}
    if ($ishome==""){$ishome=0;}
    if ($iscunju==""){$iscunju=0;}
    if ($isdown==""){$isdown=0;}
	if ($ishome2==""){$ishome2=0;}
	$classshowtype=intval($classshowtype);
    $iskeshi=intval($iskeshi);
	//$descrip=dvHTMLEncode($descrip);
	$sql2="";
	if ($Purview==1){
	    $db->excu("UPDATE qh_articleclass SET fup='$fup',name='$name',classflashurl='$classflashurl',flashurl='$flashurl',list='$orderid',listorder='$listorder',jumpurl='$jumpurl',classtype='$classtype',classshowtype='$classshowtype',ispage='$ispage',iskeshi='$iskeshi',ishome='$ishome',iscunju='$iscunju',isdown='$isdown',ishome2='$ishome2',keywords='$keywords',descrip='$descrip' $sql2 $SQL WHERE fid='$fid' ");
		//删除原始文件
	    if($classflashurl!=$oldclassflashurl){
		    @unlink("../$webdb[updir]/$oldclassflashurl");
		}
	    if($flashurl!=$oldflashurl){
		    @unlink("../$webdb[updir]/$oldflashurl");
		}
    }else{
	    $db->excu("UPDATE qh_articleclass SET fup='$fup',name='$name',list='$orderid',classshowtype='$classshowtype',listorder='$listorder',jumpurl='$jumpurl',ispage='$ispage',iskeshi='$iskeshi',ishome='$ishome',iscunju='$iscunju',isdown='$isdown',ishome2='$ishome2',descrip='$descrip' $sql2 $SQL WHERE fid='$fid' ");
	}
	//修改栏目名称之后,文章的也要跟着修改
	if($rs_fid[name]!=$name)
	{
		$db->excu(" UPDATE qh_article SET fname='$name' WHERE fid='$fid' ");
	}
	
	
	mod_sort_class("qh_articleclass",0,0);		//更新class
	mod_sort_sons("qh_articleclass",0);			//更新sons
	jump("修改成功","Admin_article_class.php");
}

elseif ($action=="delete"){
//删除分类
    if ($fid=="" || empty($fid)){
  	  $fun->popmassage("请选择要修改的栏目！","","popback");
	  exit();
    }
    if (strpos("#**#$fiddbadmin2","|||$fid|||")<=0){
		$fun->popmassage("您没有对所选分类的管理权限","","popback");
		exit();
    }
	$fup=$db->get_one("select fup from qh_articleclass where fid='$fid'");
	if ($Purview!=1 and intval($fup)==0){
  	  $fun->popmassage("您没有对此分类的管理权限，系统将自动返回！","","popback");
	  exit();
	}
	$db->excu(" DELETE FROM `qh_articleclass` WHERE fid='$fid' ");
	$db->excu(" DELETE FROM `qh_article` WHERE fid='$fid' ");

	mod_sort_class("qh_articleclass",0,0);		//更新class
	mod_sort_sons("qh_articleclass",0);			//更新sons

	jump("删除成功","Admin_article_class.php");
}
elseif($action=="editlist")
{
  //批量修改排序

	foreach( $order AS $key=>$value){
    if (strpos("#**#$fiddbadmin2","|||$key|||")<=0){
		$fun->popmassage("您没有对所选分类的管理权限","","popback");
		exit();
    }
	$fup=$db->get_one("select fup from qh_articleclass where fid='$key'");
	if ($Purview!=1 and intval($fup)==0){
  	  $fun->popmassage("您没有对此分类的管理权限，系统将自动返回！","","popback");
	  exit();
	}
		$db->excu("UPDATE qh_articleclass SET list='$value' WHERE fid='$key' ");
	}
	mod_sort_class("qh_articleclass",0,0);		//更新class
	mod_sort_sons("qh_articleclass",0);			//更新sons

	jump("修改成功","$FROMURL",1);
}
elseif ($action=="toget"){
	$selectname_1=$Guidedb->Select("qh_articleclass",'fid',"","prview");
	$selectname_2=$Guidedb->Select("qh_articleclass",'nfid',"","prview");
?>
<div class="n_div" style="width:100%;">
<table width='100%' cellspacing='1' cellpadding='3'  class="table_southidc">
  <form name="form1" method="post" action="Admin_article_class.php?action=savetoget">
    <tr class="head"> 
      <td colspan="2">&nbsp; </td>
    </tr>
    <tr class="list"> 
      <td width='51%' align="right">请选择源分类：</td>
      <td width='49%'><?php echo $selectname_1?></td>
    </tr>
    <tr class="list"> 
      <td width='51%' align="right"> 请选择目标分类：</td>
      <td width='49%'><?php echo $selectname_2?></td>
    </tr>
    <tr class="list"> 
      <td width='51%'> 
        <div align='right'></div>
      </td>
      <td width='49%'> 
        <input class=mmcinb type='submit' name='Submit' value='提交数据'>
      </td>
    </tr>
  </form>
</table>
</div>
<?php
}
elseif ($action=="savetoget"){
	if(!$fid){
  	  $fun->popmassage("请选择一个源栏目！","","popback");
	  exit();
	}elseif(!$nfid){
  	  $fun->popmassage("请选择一个目标栏目！","","popback");
	  exit();
	}
	if($fid==$nfid){
  	  $fun->popmassage("出错了，栏目本身不能合并为自己,请选择合并到其他栏目去吧！","","popback");
	  exit();
	}
    if (strpos("#**#$fiddbadmin2","|||$fid|||")<=0){
		$fun->popmassage("您没有对所选分类的管理权限","","popback");
		exit();
    }
    if (strpos("#**#$fiddbadmin2","|||$nfid|||")<=0){
		$fun->popmassage("您没有对目标分类的管理权限","","popback");
		exit();
    }
	$fup=$db->get_one("select fup from qh_articleclass where fid='$nfid'");
	if ($Purview!=1 and intval($fup)==0){
  	  $fun->popmassage("您没有对此目标分类的管理权限，系统将自动返回！","","popback");
	  exit();
	}

	$db->excu("UPDATE qh_article SET fid='$nfid' WHERE fid='$fid'");
	$db->excu("DELETE FROM qh_articleclass WHERE fid='$fid'");
	mod_sort_class("qh_articleclass",0,0);		//更新class
	mod_sort_sons("qh_articleclass",0);			//更新sons
	jump("操作完毕","$FROMURL",1);
}
elseif ($action=="add"){
?>
<div class="n_div" style="width:100%;">
<table width=100% cellspacing=1 cellpadding=3  class="table_southidc">
  <tr class="head" align="center"> 
    <td colspan="2">添加栏目</td>
  </tr>
  <?php 
  ?>
    <form name="form" method="post" action="Admin_article_class.php?action=saveadd" enctype="multipart/form-data" onSubmit="return checkclass();">
    <tr class="list"> 
      <td width="30%" align="right">栏目名称:</td>
      <td width="70%"><input type=text name="name" size=20></td>
    </tr>
    <tr class="list"> 
      <td align="right">所属分类:</td>
      <td><?php
		$sort_fup=$Guidedb->Select("qh_articleclass ","fup",$fid,"prview","","",3);
		echo $sort_fup;
		 ?>(不选择将成为一级分类) </td>
    </tr>
    <tr class="list"> 
      <td align="right">关 键 字:</td>
      <td><input type=text name="keywords" size=70></td>
    </tr>
    <tr class="list"> 
      <td align="right">栏目简介:</td>
      <td><textarea name="descrip" cols="70" rows="3"><?php echo $rsdb[descrip] ?></textarea></td>
    </tr>
    <tr class="list"> 
      <td align="right">栏目排序:</td>
      <td><input type=text name="orderid" size=20></td>
    </tr>
    <tr class="list"> 
      <td align="right">栏目类型:</td>
      <td><select name="classtype">
	  <option value="0" <?php echo $classtype[0]?>>列表</option>
	  <option value="1" <?php echo $classtype[1]?>>图片</option>
	  <option value="4" <?php echo $classtype[4]?>>图片列表</option>
	  <option value="2" <?php echo $classtype[2]?>>单页</option>
	  <option value="3" <?php echo $classtype[3]?>>外链</option>
	  </select>	  </td>
    </tr>
    <!--<tr class="list"> 
      <td align="right">栏目样式:</td>
      <td><select name="classshowtype">
	  <option value="0" <?php echo $classshowtype[0]?>>单列</option>
	  <option value="1" <?php echo $classshowtype[1]?>>双列</option>
	  </select></td>
    </tr>-->
    <tr class="list"> 
      <td align="right">二级页排序:</td>
      <td><input type=text name="listorder" size=20 value="<?php echo $rsdb["listorder"]?>"></td>
    </tr>
    <tr class="list"> 
      <td align="right">外链地址:</td>
      <td><input type=text name="jumpurl" size=50 value=""><br />栏目选择外链此链接有效</td>
    </tr>
	<tr class="list"> 
      <td align="right">其它属性:</td>
      <td>
        <input type="checkbox" name="ishome2" value="1" class="input_check" />二级主页&nbsp;<input type="checkbox" name="isdown" value="1" <?php if($rsdb[isdown]==1){echo "checked";}?> class="input_check"/>在线下载</td>
    </tr>
    <tr class="list"> 
      <td colspan="2" height="25" align="center"><input type=submit value="提 交" name="submit"></td>
    </tr>
    </form>
</table>
</div><? }
elseif ($action=="" || empty($action)){
	$fid=intval($fid);
	$sortdb=array();
	list_allsort($fid,"","","prview");

	if($fid){
		$rsdb=$db->get_one(" SELECT * FROM qh_articleclass WHERE fid='$fid' ");
	}
?>
<div class="n_div" style="width:100%;">
<form name="formlist" method="post" action="index.php?lfj=$lfj&action=editlist">
<table width=100% cellspacing=1 cellpadding=3  class="table_southidc">
  <tr> 
    <td class="head" bgcolor="#EAEAEA">编辑栏目</td>
  </tr>
  <tr class="list"> 
      <td align="left"> 
        <ul>
          <li>注意:<font color="#FF0000">栏目排序的规则是,数值大的排在前面,只能是同一级的进行排序</font></li>
        </ul> 
      </td>
  </tr>
</table>
<table width="100%" border="0" cellspacing="1" cellpadding="1" class="table_southidc">
  <tr align="center" class="head"> 
    <td width="4%">ID</td>
    <td  width="40%">栏目名称</td>
    <td width="6%">排序</td>
    <td width="15%">设置</td>
<!--	<td width="6%">频道</td>
-->    <td width="6%">删除</td>
  </tr>
<?php
foreach($sortdb AS $key=>$rs){?>
  <tr class="list" onMouseOver="this.className='list2'" onMouseOut="this.className='list'"> 
    <td width="4%" align="center"><b><font color="#FF0000"><?php echo $rs[fid]?></font></b></td>
    <td width="40%">
     <?php echo $rs[icon]?>【<?php echo $rs[name]?>】</td>
    <td width="6%" align="center"> 
      <?php if ($Purview==1 or $rs[fup]!=0){?><input type='text' name='order[<?php echo $rs[fid]?>]' value='<?php echo $rs['list'] ?>' size='5'><?php }?></td>
    <td width="15%" align="center"><?php if($rs["class"]<3){?>[<a href='?action=add&fid=<?php echo $rs[fid]?>'><font color=red>添加分类</font></a>]<?php }
	if ($Purview==1 or $rs[fup]!=0){?>[<a href='?action=editclass&fid=<?php echo $rs[fid]?>'><font color=red>修改</font></a>]<?php } ?></td>
	<td width="6%" align="center"><?php if ($Purview==1 or $rs[fup]!=0){?>[<a href="?action=delete&fid=<?php echo $rs[fid] ?>" onClick="return confirm('你确实要删除吗?不可恢复');">删除</a>]<?php }?></td>
  </tr>
<?php }?>
</table>
</form>
<div align="center">
   
  <a href="javascript:" onClick="CheckAll('all')">全选</a>/<a href="javascript:" onClick='CheckAll()'>反选</a> <input type="button" name="Submit2" value="修改栏目排序" onClick="postlist('list')">
<SCRIPT LANGUAGE="JavaScript">
<!--
function postlist(t){
	if(t=='list'){
		document.formlist.action="Admin_article_class.php?action=editlist";
	}
	document.formlist.submit();
}
function CheckAll(va){
	form=document.formlist
	for (var i=0;i<form.elements.length;i++){
		var e = form.elements[i];
		if(va=='all'){
			e.checked = true;
		}else{
			e.checked == true ? e.checked = false : e.checked = true;
		}
	}
}

</SCRIPT>
		  </div><br>
</div>
<?php
}?>
</div>
<?
require("foot.php");
?>
</body>
</html>
