<?php
@session_start();
$PurviewLevel=3;
$CheckChannelID=0;  
$PurviewLevel_Others="ModifyPwd";

require("../inc/common.inc.php");
require_once(dirname(__FILE__)."/"."Admin_ChkPurview.php");

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=gb2312" />
<meta http-equiv="X-UA-Compatible" content="IE=EmulateIE7" />
<TITLE>铭讯EOS网站后台管理</TITLE>
<Meta name="Keywords" Content="济南网站建设 济南网站制作 山东网站建设 济南铭讯软件有限公司">
<Meta name="Description" Content="济南网站制作：铭讯软件专业服务优秀企业，致力于网站建、设网站推广、网络营销一体的全访位网络服务商">
<link rel="stylesheet" href="images/css.css" type="text/css">
</head>
<?php require("head.php");?>
<div class="boxdiv">
<table width="100%" border="0" cellpadding="0" cellspacing="0" class="n_toptable">
<tr><td align="center" colspan="2">
    <table border="0" cellpadding="0" cellspacing="0" class="n_toptabale_s">
    <tr>
      <td width="14" class="lefttd"></td>
      <td  class="centertd">修改我的密码</td>
      <td class="righttd"></td>
     </tr>
    </table>
</td></tr>
</table>
<!--详细信息开始-->
<div class="n_div" style="width:100%;">
<?php
if ($action=="save"){
//修改密码
	if($postdb[oldpass]=="" or empty($postdb[oldpass])){
		$fun->popmassage("旧密码不能为空，请重新输入!","","popback");
		exit();
	} 
    $postdb[oldpass]=md5($postdb[oldpass]);
	$rsdb=$db->excu("SELECT password FROM members   WHERE username ='$postdb[username]' and password='$postdb[oldpass]'");
    $rsnum=$db->num_rows($rsdb);
	if($rsnum<=0){
		$fun->popmassage("旧密码不正确，请重新输入!","","popback");
		exit();
	}
	if($postdb[password]=="" or empty($postdb[password])){
		$fun->popmassage("新密码不能为空，请重新输入!","","popback");
		exit();
	} 
	
	if($postdb[password2]=="" or empty($postdb[password2])){
		$fun->popmassage("确认密码不能为空，请重新输入!","","popback");
		exit();
	} 
	
	if($postdb[password]!=$postdb[password2]){
		$fun->popmassage("两次输入密码不一致，请重新输入!","","popback");
		exit();
	} 
	$postdb[password]=md5($postdb[password]);
//	echo "update members  set password='$postdb[password]' where  username ='$postdb[username]' and password='$postdb[oldpass]'";
//	exit();
	$db->excu("update members set password='$postdb[password]' where  username ='$postdb[username]' and password='$postdb[oldpass]'");
		$fun->popmassage("操作成功!","Admin_chpass.php","popgotourl");
}
else{

?>
<table width="100%" border="0" cellspacing="1" cellpadding="3" class="table_southidc">
<form name="all" action="Admin_chpass.php" method="post" onSubmit="return dianji();" id="all">
<tr class="head"><td colspan="2" align="center">修改我的密码</td></tr>
<tr class="list">
  <td height="30" align="right">　用户名：</td>
  <td align="left"><?php echo $_SESSION['mxadmin']['username'];?><input type="hidden" name="postdb[username]" value="<?php echo $_SESSION['mxadmin']['username'];?>"/></td>
</tr>
<tr class="list">
  <td height="30" align="right">　请输入旧密码：</td>
  <td align="left">
					    <label><input type="password" name="postdb[oldpass]" id="oldpass" maxlength="30" style="width:180px;"/></label>
		  </td>
</tr>
<tr class="list">
  <td height="30" align="right">　密码（至少6位）：</td>
  <td align="left"><input type="password" name="postdb[password]" id="password" maxlength="30" style="width:180px;"/></td>
</tr>
<tr class="list">
  <td height="30" align="right">　确认密码：</td>
  <td align="left"><input type="password" name="postdb[password2]" id="password2" maxlength="30" style="width:180px;"/></td>
</tr>
<tr class="list"><td colspan="2" align="center"><input type="hidden" name="action" value="save"><input type="submit" value="修改密码">&nbsp;&nbsp;&nbsp;&nbsp;<input type="reset" value="重新填写"></td></tr>
</form>
</table>
<script language="javascript">
 function dianji(){
  
    if(document.all.oldpass.value==""){
	  alert("请输入旧密码！");
	  document.all.oldpass.focus();
	  return false;
	}else{
	 if(document.all.oldpass.value.length<4){
	    alert("旧密码不能少于4个字符!"); 
	    document.all.oldpass.focus();
		return false;
	 }
	}
	if(document.all.password.value==""){
	  alert("请输入新密码！");
	  document.all.password.focus();
	  return false;
	}else{
	 if(document.all.password.value.length<6){
	    alert("新密码不能少于6个字符!"); 
	    document.all.password.focus();
		return false;
	 }
	}
	if(document.all.password2.value==""){
	  alert("请输入确认密码！");
	  document.all.password2.focus();
	  return false;
	}else{
	  if(document.all.password2.value==document.all.password.value){
	   
	  }else{
	     alert("两次输入的密码不同！");
	     document.all.password2.focus();
		 return false;
	  }
	}
}
//重新填写内容
function setreset(){
document.all.oldpass.value="";
document.all.password.value="";
document.all.password2.value="";	
}
</script>
<?php }?>
</div>
</div>
<?php
require("foot.php");?>
</body>
</html>
