<?php
@session_start();
$PurviewLevel=2;
$CheckChannelID=0;  
$PurviewLevel_Others="guanggao";

require("../inc/common.inc.php");
require_once(dirname(__FILE__)."/"."Admin_ChkPurview.php");

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=gb2312" />
<meta http-equiv="X-UA-Compatible" content="IE=EmulateIE7" />
<TITLE>铭讯EOS网站后台管理</TITLE>
<Meta name="Keywords" Content="济南网站建设 济南网站制作 山东网站建设 济南铭讯软件有限公司">
<Meta name="Description" Content="济南网站制作：铭讯软件专业服务优秀企业，致力于网站建、设网站推广、网络营销一体的全访位网络服务商">
<link rel="stylesheet" href="images/css.css" type="text/css">
</head>
<SCRIPT LANGUAGE="JavaScript">
<!--
function upfile(url,name,size,p){
	//if(p=='pic'){
		document.getElementById(p).value=url;
	//}else{
	//	document.getElementById("flashurl").value=url;
	//}
	
}
//-->
</SCRIPT>
<body> 
<?php require("head.php");
$array_adtype=array("index1"=>"首页大广告","index_x1"=>"首页小广告一","index_x2"=>"首页小广告二","nei_left1"=>"内页左侧广告","baoad"=>"三中报底部");
?>
<div class="boxdiv">
<table width="100%" border="0" cellpadding="0" cellspacing="0" class="n_toptable">
<tr><td align="center" colspan="2">
    <table border="0" cellpadding="0" cellspacing="0" class="n_toptabale_s">
    <tr>
      <td width="14" class="lefttd"></td>
      <td  class="centertd">广告管理</td>
      <td class="righttd"></td>
     </tr>
    </table>
</td></tr>
</table>
<table border="0" cellpadding="0" cellspacing="1" class="table_southidc" style="width:100%">
  <tr class="list"> 
    <td width="10%"><strong>管理导航：</strong></td>
    <td align="left">&nbsp;
	<a href="Admin_ad.php">广告信息管理</a>丨<a href="Admin_ad.php?actions=add">添加广告信息</a>丨<a href="Admin_ad.php?yz=1">已审核广告</a>丨<a href="Admin_ad.php?yz=0">未审核广告</a></td>
  </tr>
</table>
<?php
if($actions=="yz")
{
	$db->excu("UPDATE qh_ad SET `yz`='$ifyz' WHERE id='$id'");
	jump("修改成功","$FROMURL",0);
}
elseif($actions=="del"){
//删除广告
	if(!$iddb&&!$id){
		$fun->popmassage("请选择一篇文章","","popback");
		exit();
	}
	elseif(is_array($iddb))
	{
		foreach($iddb AS $key=>$value){
			$db->excu("DELETE FROM qh_ad WHERE id='$key' ");
		}
	}elseif($id){
		$db->excu("DELETE FROM qh_ad WHERE id='$id' ");
	}
	$url=$fromurl?$fromurl:$FROMURL;
	jump("操作成功",$url,1);
}
elseif ($actions=="add"){
	$rsdb[type]='word';
	$rsdb[keywords]="AD_".rand(1,9999);
	$_pictarget[blank]=$_wordtarget[blank]=" checked ";
	$autoyz[1]=' checked ';
?>
<div class="n_div" style="width:100%;">
<table width="100%" border="0" cellspacing="1" cellpadding="3" class="table_southidc">
  <form name="form1" method="post" action="Admin_ad.php?actions=saveadd">
    <tr class="head"> 
      <td colspan="2">添加新广告</td>
    </tr>
    <tr class="list"> 
      <td width="16%">广告位名称: </td>
      <td width="84%"> 
        <input type="text" name="postdb[name]" value="" size="40">      </td>
    </tr>
    <tr class="list"> 
      <td width="16%">广告位置:</td>
      <td width="84%">
	    <select name="postdb[adtype]">
		  <option value="">选择广告位置</option>
		  <?php 
		  foreach ($array_adtype as $key =>$value){
		    echo "<option value=\"$key\">$value</option>";
		  }
		  ?>
		</select>
	  </td>
    </tr>
    <tr class="list"> 
      <td width="16%">审核:</td>
      <td width="84%"> 
        <input type="radio" name="yz[]" value="1" class="input_check" checked="checked">
       通过审核 
        <input type="radio" name="yz[]" value="0" class="input_check">
        未通过审核</td>
    </tr>
    <tr class="list"> 
      <td colspan="2"> 
        <input type="radio" name="postdb[type]" value="word" id="word_bt" onClick="choose_type('word')" class="input_check">
        文字广告位 
        <input type="radio" name="postdb[type]" value="pic" id="pic_bt" onClick="choose_type('pic')" class="input_check">
        图片广告位 
        <input type="radio" name="postdb[type]" value="swf" id="swf_bt" onClick="choose_type('swf')" class="input_check">
        FLASH广告位 
        <input type="radio" name="postdb[type]" value="code" id="code_bt" onClick="choose_type('code')" class="input_check">
        代码广告位</td>
    </tr>
    <tr class="list"> 
      <td colspan="2"> 
        <table width="100%" border="0" cellspacing="0" cellpadding="3" id="worddiv" style="display:none;">
          <tr> 
            <td width="16%">文字内容:</td>
            <td width="84%"> 
              <input type="text" name="cdb[word]" value="">            </td>
          </tr>
          <tr> 
            <td width="16%">文字链接地址:</td>
            <td width="84%"> 
              <input type="text" name="wordlinkurl" value="">            </td>
          </tr>
          <tr> 
            <td width="16%">打开方式:</td>
            <td width="84%"> 
              <input type="radio" name="wordtarget" value="blank" <?php echo $_wordtarget[blank]?>  class="input_check">
              新窗口打开 
              <input type="radio" name="wordtarget" value="self" <?php echo $_wordtarget[self] ?> class="input_check">
              本窗口打开 </td>
          </tr>
        </table>
        <table width="100%" border="0" cellspacing="0" cellpadding="0" id="picdiv" style="display:none;">
          <tr> 
            <td width="16%">图片地址:</td>
            <td width="84%"> 
              <input type="text" name="cdb[picurl]" size="60" value="" id="imgurl">
              <a href="javascript:show_uplode('upfile','ad','imgurl')"><font color="#FF0000">点击上传图片</font></a>            </td>
          </tr>
          <tr> 
            <td width="16%">图片链接地址:</td>
            <td width="84%"> 
              <input type="text" name="cdb[linkurl]" size="60" value="">            </td>
          </tr>
          <tr> 
            <td width="16%">图片宽度:</td>
            <td width="84%"> 
              <input type="text" name="picwidth" value="" size="5" id="imgwidth">
              像素</td>
          </tr>
          <tr> 
            <td width="16%">图片高度:</td>
            <td width="84%"> 
              <input type="text" name="picheight" value="" size="5" id="imgheight">
              像素</td>
          </tr>
          <tr> 
            <td width="16%">打开方式:</td>
            <td width="84%"> 
              <input type="radio" name="pictarget" value="blank" $_pictarget[blank]  class="input_check">
              新窗口打开 
              <input type="radio" name="pictarget" value="self" $_pictarget[self]  class="input_check">
              本窗口打开</td>
          </tr>
        </table>
        <table width="100%" border="0" cellspacing="0" cellpadding="0" id="flashdiv" style="display:none;">
          <tr> 
            <td width="16%">FLASH地址:</td>
            <td width="84%"> 
              <input type="text" name="cdb[flashurl]" size="60" value="" id="flashurl">
              <a href="javascript:show_uplode('upfile','ad','flashurl')"><font color="#FF0000">点击上传FLASH</font></a>            </td>
          </tr>
          <tr> 
            <td width="16%">FLASH宽度:</td>
            <td width="84%"> 
              <input type="text" name="swfwidth" value="" size="5">
              像素</td>
          </tr>
          <tr> 
            <td width="16%">FLASH高度:</td>
            <td width="84%"> 
              <input type="text" name="swfheight" value="" size="5">
              像素</td>
          </tr>
        </table>
        <table width="100%" border="0" cellspacing="0" cellpadding="0" id="codediv" style="display:none;">
          <tr> 
            <td width="16%">广告代码:</td>
            <td width="84%"><textarea name="cdb[code]" cols="70" rows="7"></textarea> </td>
          </tr>
        </table>      </td>
    </tr>
    <tr class="list"> 
      <td colspan="2" align="center"> 
<SCRIPT LANGUAGE="JavaScript">
<!--
function choose_type(type){
	if(type=="word"){
		document.getElementById("word_bt").checked=true;
		document.getElementById("pic_bt").checked=false;
		document.getElementById("swf_bt").checked=false;
		document.getElementById("code_bt").checked=false;

		document.getElementById("worddiv").style.display='';
		document.getElementById("picdiv").style.display='none';
		document.getElementById("flashdiv").style.display='none';
		document.getElementById("codediv").style.display='none';
	}else if(type=="pic"){
		document.getElementById("word_bt").checked=false;
		document.getElementById("pic_bt").checked=true;
		document.getElementById("swf_bt").checked=false;
		document.getElementById("code_bt").checked=false;

		document.getElementById("worddiv").style.display='none';
		document.getElementById("picdiv").style.display='';
		document.getElementById("flashdiv").style.display='none';
		document.getElementById("codediv").style.display='none';
	}else if(type=="swf"){
		document.getElementById("word_bt").checked=false;
		document.getElementById("pic_bt").checked=false;
		document.getElementById("swf_bt").checked=true;
		document.getElementById("code_bt").checked=false;

		document.getElementById("worddiv").style.display='none';
		document.getElementById("picdiv").style.display='none';
		document.getElementById("flashdiv").style.display='';
		document.getElementById("codediv").style.display='none';
	}else if(type=="code"){
		document.getElementById("word_bt").checked=false;
		document.getElementById("pic_bt").checked=false;
		document.getElementById("swf_bt").checked=false;
		document.getElementById("code_bt").checked=true;

		document.getElementById("worddiv").style.display='none';
		document.getElementById("picdiv").style.display='none';
		document.getElementById("flashdiv").style.display='none';
		document.getElementById("codediv").style.display='';
	}else if(type=="duilian"){
		document.getElementById("word_bt").checked=false;
		document.getElementById("pic_bt").checked=false;
		document.getElementById("swf_bt").checked=false;
		document.getElementById("code_bt").checked=false;

		document.getElementById("worddiv").style.display='none';
		document.getElementById("picdiv").style.display='none';
		document.getElementById("flashdiv").style.display='none';
		document.getElementById("codediv").style.display='none';
	}
}
//-->
</SCRIPT>
        <input type="submit" name="Submit" value="提 交">
        <script language="javascript">
function cutimg() 
{
	var img=document.getElementById("imgurl").value;
	var cw=document.getElementById("imgwidth").value;
	var ch=document.getElementById("imgheight").value;
	if(cw==''){
		document.getElementById("imgwidth").focus();
		alert("请设置好图片宽度");return false;
	}
	if(ch==''){
		document.getElementById("imgheight").focus();
		alert("请设置好图片高度");return false;
	}
if(img!=''){
	if(img.indexOf("http://")==-1){
		img="$webdb[www_url]/$webdb[updir]/"+img;
	}
	window.open("cutimg.php?job=cutimg&width="+cw+"&height="+ch+"&srcimg="+img,'',"width=480,height=430")
}else{
	document.getElementById("imgurl").focus();
	alert('图片地址不存在');
}
}
</script>

<SCRIPT LANGUAGE="JavaScript">
<!--
function if_sale(v){
	if(v=='1'){
		get_obj('saletr1').style.display='';
		get_obj('saletr2').style.display='';
		get_obj('nosaletr1').style.display='none';
	}else{
		get_obj('saletr1').style.display='none';
		get_obj('saletr2').style.display='none';
		get_obj('nosaletr1').style.display='';
	}
}
//-->
</SCRIPT>      </td>
    </tr>
  </form>
</table>
</div>
<?php 
}
elseif ($actions=="modi"){
//修改广告
	$rsdb=$db->get_one("SELECT * FROM `qh_ad` WHERE id='$id'");
	@extract(unserialize($rsdb[adcode]));
	$code=stripslashes($code);

	$pictarget||$pictarget='blank';
	$wordtarget||$wordtarget='blank';
	$_pictarget[$pictarget]=" checked ";
	$_wordtarget[$wordtarget]=" checked ";
	$yz[$rsdb[yz]]=" checked";
	$type[$rsdb[type]]=" checked";
	if ($gotourl==""){$gotourl=url_encode($_SERVER['HTTP_REFERER']);}
?>
<script language="javascript">
function check(){
  if (document.form1.postdb[name].value==""){
    alert('链接名称不能为空！');
	document.form1.postdb[name].focus();
	return false;
  }
  if (document.form1.postdb[url].value==""){
    alert('链接地址不能为空！');
	document.form1.postdb[url].focus();
	return false;
  }
  
}
</script>
<div class="n_div" style="width:100%;">
<table width='100%' cellspacing='1' cellpadding='3' class="table_southidc">
  <form name="form1" method="post" action="Admin_ad.php?actions=savemodi" onSubmit="return  check();">
    <tr class="head"> 
      <td colspan="3"> 
        修改广告信息</td>
    </tr>
    <tr class="list"> 
      <td width="16%">广告位名称: </td>
      <td width="84%"> 
        <input type="text" name="postdb[name]" value="<?php echo $rsdb[name]?>" size="40">
      </td>
    </tr>
    <tr class="list"> 
      <td width="16%">广告位置:</td>
      <td width="84%"> 
	    <select name="postdb[adtype]">
		  <option value="">选择广告位置</option>
		  <?php 
		  foreach ($array_adtype as $key =>$value){
		    if ($key==$rsdb[adtype]){
		      echo "<option value=\"$key\" selected>$value</option>";
			}
			else{
		      echo "<option value=\"$key\">$value</option>";
			}
		  }
		  ?>
		</select>
      </td>
    </tr>
    <tr class="list"> 
      <td width="16%">审核:</td>
      <td width="84%"> 
        <input type="radio" name="yz[]" value="1" <?php echo $yz[1]?> class="input_check" checked="checked">
       通过审核 
        <input type="radio" name="yz[]" value="0" <?php echo $yz[0]?> class="input_check">
        未通过审核</td>
    </tr>
    <tr class="list"> 
      <td colspan="2"> 
        <input type="radio" name="postdb[type]" value="word" id="word_bt" onClick="choose_type('word')" class="input_check" <?php echo $type[word]?>>
        文字广告位 
        <input type="radio" name="postdb[type]" value="pic" id="pic_bt" onClick="choose_type('pic')" class="input_check"<?php echo $type[pic]?>>
        图片广告位 
        <input type="radio" name="postdb[type]" value="swf" id="swf_bt" onClick="choose_type('swf')" class="input_check"<?php echo $type[swf]?>>
        FLASH广告位 
        <input type="radio" name="postdb[type]" value="code" id="code_bt" onClick="choose_type('code')" class="input_check"<?php echo $type[code]?>>
        代码广告位 
 </td>
    </tr>
    <tr class="list"> 
      <td colspan="2"> 
        <table width="100%" border="0" cellspacing="0" cellpadding="3" id="worddiv" style="<?php if ($rsdb[type]!="word"){echo "display:none;";}?>">
          <tr> 
            <td width="16%">文字内容:</td>
            <td width="84%"> 
              <input type="text" name="cdb[word]" value="<?php echo $word?>">
            </td>
          </tr>
          <tr> 
            <td width="16%">文字链接地址:</td>
            <td width="84%"> 
              <input type="text" name="wordlinkurl" value="<?php echo $linkurl?>">
            </td>
          </tr>
          <tr> 
            <td width="16%">打开方式:</td>
            <td width="84%"> 
              <input type="radio" name="wordtarget" value="blank" <?php echo $_wordtarget[blank]?> class="input_check">
              新窗口打开 
              <input type="radio" name="wordtarget" value="self" <?php echo $_wordtarget[self]?> class="input_check">
              本窗口打开 </td>
          </tr>
        </table>
        <table width="100%" border="0" cellspacing="0" cellpadding="0" id="picdiv" style="<?php if ($rsdb[type]!="pic"){echo "display:none;";}?>">
          <tr> 
            <td width="16%">图片地址:</td>
            <td width="84%"> 
              <input type="text" name="cdb[picurl]" size="60" value="<?php echo $picurl?>" id="imgurl">
              <a href="javascript:show_uplode('upfile','ad','imgurl')"><font color="#FF0000">点击上传图片</font></a> 
            </td>
          </tr>
          <tr> 
            <td width="16%">图片链接地址:</td>
            <td width="84%"> 
              <input type="text" name="cdb[linkurl]" size="60" value="<?php echo $linkurl?>">
            </td>
          </tr>
          <tr> 
            <td width="16%">图片宽度:</td>
            <td width="84%"> 
              <input type="text" name="picwidth" value="<?php echo $width?>" size="5" id="imgwidth">
              像素</td>
          </tr>
          <tr> 
            <td width="16%">图片高度:</td>
            <td width="84%"> 
              <input type="text" name="picheight" value="<?php echo $height?>" size="5" id="imgheight">
              像素</td>
          </tr>
          <tr> 
            <td width="16%">打开方式:</td>
            <td width="84%"> 
              <input type="radio" name="pictarget" value="blank" <?php echo $_pictarget[blank]?> class="input_check">
              新窗口打开 
              <input type="radio" name="pictarget" value="self" <?php echo $_pictarget[self]?> class="input_check">
              本窗口打开</td>
          </tr>
        </table>
        <table width="100%" border="0" cellspacing="0" cellpadding="0" id="flashdiv" style="<?php if ($rsdb[type]!="swf"){echo "display:none;";}?>">
          <tr> 
            <td width="16%">FLASH地址:</td>
            <td width="84%"> 
              <input type="text" name="cdb[flashurl]" size="60" value="<?php echo $flashurl?>" id="flashurl">
              <a href="javascript:show_uplode('upfile','ad','flashurl')"><font color="#FF0000">点击上传FLASH</font></a> 
            </td>
          </tr>
          <tr> 
            <td width="16%">FLASH宽度:</td>
            <td width="84%"> 
              <input type="text" name="swfwidth" value="<?php echo $width?>" size="5">
              像素</td>
          </tr>
          <tr> 
            <td width="16%" height="24">FLASH高度:</td>
            <td width="84%"> 
              <input type="text" name="swfheight" value="<?php echo $height?>" size="5">
              像素</td>
          </tr>
        </table>
        <table width="100%" border="0" cellspacing="0" cellpadding="0" id="codediv" style="<?php if ($rsdb[type]!="code"){echo "display:none;";}?>">
          <tr> 
            <td width="16%">广告代码:</td>
            <td width="84%"> 
              <textarea name="cdb[code]" cols="70" rows="7"><?php echo $code?></textarea>
            </td>
          </tr>
        </table>
      </td>
    </tr>
    <tr class="list"> 
      <td colspan="2" align="center"> 
        <SCRIPT LANGUAGE="JavaScript">
<!--
function choose_type(type){
	if(type=="word"){
		document.getElementById("word_bt").checked=true;
		document.getElementById("pic_bt").checked=false;
		document.getElementById("swf_bt").checked=false;
		document.getElementById("code_bt").checked=false;

		document.getElementById("worddiv").style.display='';
		document.getElementById("picdiv").style.display='none';
		document.getElementById("flashdiv").style.display='none';
		document.getElementById("codediv").style.display='none';
	}else if(type=="pic"){
		document.getElementById("word_bt").checked=false;
		document.getElementById("pic_bt").checked=true;
		document.getElementById("swf_bt").checked=false;
		document.getElementById("code_bt").checked=false;

		document.getElementById("worddiv").style.display='none';
		document.getElementById("picdiv").style.display='';
		document.getElementById("flashdiv").style.display='none';
		document.getElementById("codediv").style.display='none';
	}else if(type=="swf"){
		document.getElementById("word_bt").checked=false;
		document.getElementById("pic_bt").checked=false;
		document.getElementById("swf_bt").checked=true;
		document.getElementById("code_bt").checked=false;

		document.getElementById("worddiv").style.display='none';
		document.getElementById("picdiv").style.display='none';
		document.getElementById("flashdiv").style.display='';
		document.getElementById("codediv").style.display='none';
	}else if(type=="code"){
		document.getElementById("word_bt").checked=false;
		document.getElementById("pic_bt").checked=false;
		document.getElementById("swf_bt").checked=false;
		document.getElementById("code_bt").checked=true;

		document.getElementById("worddiv").style.display='none';
		document.getElementById("picdiv").style.display='none';
		document.getElementById("flashdiv").style.display='none';
		document.getElementById("codediv").style.display='';
	}
}
choose_type('$rsdb[type]');
//-->
</SCRIPT>
		<input type="hidden" name="gotourl" value="" />
        <input type="hidden" name="id" value="<?php echo $id?>">
		<input type="hidden" name="gotourl" value="<?php echo $gotourl?>">
        <input type="submit" name="Submit" value="提 交">
        <script language="javascript">
function cutimg() 
{
	var img=document.getElementById("imgurl").value;
	var cw=document.getElementById("imgwidth").value;
	var ch=document.getElementById("imgheight").value;
	if(cw==''){
		document.getElementById("imgwidth").focus();
		alert("请设置好图片宽度");return false;
	}
	if(ch==''){
		document.getElementById("imgheight").focus();
		alert("请设置好图片高度");return false;
	}
if(img!=''){
	if(img.indexOf("http://")==-1){
		img="$webdb[www_url]/$webdb[updir]/"+img;
	}
	window.open("cutimg.php?job=cutimg&width="+cw+"&height="+ch+"&srcimg="+img,'',"width=480,height=430")
}else{
	document.getElementById("imgurl").focus();
	alert('图片地址不存在');
}
}
</script>
      </td>
    </tr>
  </form>
</table>
</div>
<?php 
}

elseif($actions=="saveadd")
{
//添加广告信息
  if($postdb[name]=="" || empty($postdb[name])){
		$fun->popmassage("请填写广告名称","","popback");
		exit();
  }
	if($postdb[type]=='word'){
		$cdb[linkurl]=$wordlinkurl;
		$cdb[wordtarget]=$wordtarget;
	}elseif($postdb[type]=='pic'){
		$cdb[width]=$picwidth;
		$cdb[height]=$picheight;
		$cdb[pictarget]=$pictarget;
	}elseif($postdb[type]=='swf'){
		$cdb[width]=$swfwidth;
		$cdb[height]=$swfheight;
	}
	$cdb[code]=stripslashes($cdb[code]);
	$postdb[adcode]=addslashes(serialize($cdb));
	$db->excu("INSERT INTO `qh_ad` (adtype,`name` , `keywords`  , `adcode` , `type` ,yz) 
				VALUES ('$postdb[adtype]',
		'$postdb[name]','$postdb[keywords]','$postdb[adcode]','$postdb[type]','$yz[0]'
		)");
	jump("添加成功","Admin_ad.php?actions=add",1);
}
elseif ($actions=="savemodi"){
//修改广告信息
    if($id=="" || empty($id)){
		$fun->popmassage("请选择要修改的广告","","popback");
		exit();
  }
  if($postdb[name]=="" || empty($postdb[name])){
		$fun->popmassage("请填写广告名称","","popback");
		exit();
  }
	if($postdb[type]=='word'){
		$cdb[linkurl]=$wordlinkurl;
		$cdb[wordtarget]=$wordtarget;
	}elseif($postdb[type]=='pic'){
		$cdb[width]=$picwidth;
		$cdb[height]=$picheight;
		$cdb[pictarget]=$pictarget;
	}elseif($postdb[type]=='swf'){
		$cdb[width]=$swfwidth;
		$cdb[height]=$swfheight;
	}
	$cdb[code]=stripslashes($cdb[code]);
	$postdb[adcode]=addslashes(serialize($cdb));
//	echo  "UPDATE `qh_ad` SET name='$postdb[name]',keywords='$postdb[keywords]',adcode='$postdb[adcode]',type='$postdb[type]' WHERE id='$id' ";
//	exit();
	$db->excu("UPDATE `qh_ad` SET adtype='$postdb[adtype]',name='$postdb[name]',keywords='$postdb[keywords]',adcode='$postdb[adcode]',type='$postdb[type]',yz=$yz[0] WHERE id='$id' ");
	if (trim($gotourl)==""){
	$gotourl=$FROMURL;
	}else{$gotourl=url_code($gotourl);}
	jump("修改成功","$gotourl",1);
	exit();
}
else{
  $num_per_page=20;
  $page="";
  $add="yz=$yz&";
  if ($yz=="1"){
    $yzwhere=" where AD.yz='1' ";
  }
  elseif ($yz=="0"){
    $yzwhere=" where AD.yz='0' ";
  }
  $query="SELECT AD.* FROM qh_ad AD $yzwhere  ORDER BY AD.id DESC";
  $fun->page($query,$page,$add,$num_per_page);
  $result=$db->excu($query);
?>
<div class="n_div" style="width:100%;">
<table width='100%' cellspacing='1' cellpadding='3' class="table_southidc">
  <form name="form1" method="post" action="Admin_ad.php">
  <tr class="head" align="center"> 
    <td width="8%">id</td>
    <td width="30%">名 称</td>
    <td width="17%">广告位置</td>
    <td width="12%">状态</td>
    <td width="14%">广告管理</td>
  </tr>
<?php
  $i=1;
  while($rs=mysql_fetch_array($result)){
		$rs[begintime]=$rs[begintime]?date("Y-m-d H:i:s",$rs[begintime]):'';
		$rs[endtime]=$rs[endtime]?date("Y-m-d H:i:s",$rs[endtime]):'';
		$rs[yz]=$rs[yz]?"<A HREF='?actions=yz&ifyz=0&id=$rs[id]' style='color:red;' title='取消审核'>已审核</A>":"<A HREF='?actions=yz&ifyz=1&id=$rs[id]' style='color:blue;' title='审核通过'>未审核</A>";
		$listdb[]=$rs;
?>
  <tr class="list" onMouseMove="this.className='list2';" onMouseOut="this.className='list';"> 
    <td align="center"><?php echo $rs[id]?></td>
    <td><?php echo $rs[name]?></td>
    <td align="center"><?php echo $array_adtype[$rs[adtype]]?></td>
    <td align="center"><?php echo $rs[yz]?></td>
    <td align="center"><a href="Admin_ad.php?actions=modi&id=<?php echo $rs[id]?>">修改</a> 
      / <a href="Admin_ad.php?actions=del&id=<?php echo $rs[id]?>" onClick="return confirm('你确认要删除这个广告吗?')">删除</a></td>
  </tr>
<?php
}
?>
  <tr class="list">
    <td align="center" colspan="8">
	<?PHp
    $query="SELECT AD.* FROM qh_ad AD $yzwhere  ORDER BY AD.id DESC";
	echo  $fun->page($query,$page,$add,$num_per_page);
	?>
	</td>
  </tr>
  </form>
</table>
</div>
<?php
}
?>
</div>
<?php require("foot.php");?>
</body>
</html>
